This is a serverside redo of Unturned that fixes major issues in the game.

TODO:

    - Load/use collision meshes (assets)
    - Serverside collision and movement verification
    - Real module and API system
    - Communicate with clients
        - Steam packets
        - Steam master server for server info
    - Load the map
    - Types of interactable objects in the game
        - Storage/Displays
        - Doors
        - Fires
        - Claim flags
        - Oxygenators
        - Generators
        - Signs 
        - Etc
    - Scaleability
        - Instead of having something for every interactable
        - Simply use .dat file to customize
    - Automatically update assets/meshes
    - Automatically update game if necessary
    - Documentation
    - Wrapper method for steamcalls (so that they won't be used)