﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Console
{
    public class GameConsole : MonoBehaviour
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        Windows.ConsoleWindow console = new Windows.ConsoleWindow();
        Windows.ConsoleInput input = new Windows.ConsoleInput();
        void Awake()
        {
            UnturnedLogger.ConsoleInstance = this;

            console.Initialize();
            console.SetTitle("Unturned");

            input.inputString = "";
        }

        void Update()
        {
            input.Update();
        }

        void OnDestroy()
        {
            console.Shutdown();
        }

        public void Log(string message, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;

            if (System.Console.CursorLeft != 0)
                input.ClearLine();

            System.Console.WriteLine(message);
            System.Console.ForegroundColor = ConsoleColor.White;

            // If we were typing something re-add it.
            input.RedrawInputLine();
        }

        public void Log(string message, LogType type)
        {
            switch (type)
            {
                case LogType.Assert:
                    Log(message, ConsoleColor.Cyan);
                    break;
                case LogType.Exception:
                    Log(message, ConsoleColor.DarkRed);
                    break;
                case LogType.Error:
                    Log(message, ConsoleColor.Red);
                    break;
                case LogType.Warning:
                    Log(message, ConsoleColor.Yellow);
                    break;
            }
        }
#else
        Linux.ConsoleInput input = new Linux.ConsoleInput();
        void Awake()
        {
            UnturnedLogger.ConsoleInstance = this;

            input.Init();
            InvokeRepeating("Test", 0, 1);
            InvokeRepeating("Test_Two", 0.5f, 1);
        }

        void Test()
        {
            UnturnedLogger.Log("test", ConsoleColor.DarkRed);
        }

        void Test_Two()
        {
            UnturnedLogger.Log("test2", ConsoleColor.DarkBlue);
        }

        public void Log(string message, ConsoleColor color)
        {
            string OutputFileDirectory = Main.Main.ServerInstanceName + "/Console.STDOUT";
            Debug.Log(OutputFileDirectory);
            
            string postfix = "";

            if (Linux.ConsoleUtils.ConsoleColorToEscapeCode(color) != "")
                postfix = @"\033[0m";
            
            File.WriteAllText(OutputFileDirectory, Linux.ConsoleUtils.ConsoleColorToEscapeCode(color) + message + postfix );
        }

        public void Log(string message, LogType type)
        {
            switch (type)
            {
                case LogType.Assert:
                    Log(message, ConsoleColor.Cyan);
                    break;
                case LogType.Exception:
                    Log(message, ConsoleColor.DarkRed);
                    break;
                case LogType.Error:
                    Log(message, ConsoleColor.Red);
                    break;
                case LogType.Warning:
                    Log(message, ConsoleColor.Yellow);
                    break;
            }
        }
#endif
    }
}
