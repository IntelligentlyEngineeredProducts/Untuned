﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Assets.Console.Linux
{
    public class ConsoleInput
    {
        private static TextReader InputReader;

        public void Init()
        {
            string InputFileDirectory = Main.Main.ServerInstanceName + "/Console.STDIN";

            FileSystemWatcher FileSystemWatcher = new FileSystemWatcher(Main.Main.ServerInstanceName, "Console.STDIN");
            FileSystemWatcher.Changed += OnInput;
            InputReader = new StreamReader(new FileStream(InputFileDirectory, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite));
            FileSystemWatcher.EnableRaisingEvents = true;
        }

        void OnInput(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                string newline = InputReader.ReadToEnd();
                if (!String.IsNullOrEmpty(newline))
                    UnturnedLogger.Log("> " + newline, ConsoleColor.Magenta);
            }
        }
    }
}
