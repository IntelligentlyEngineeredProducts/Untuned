﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Console
{
    public static class UnturnedLogger
    {
        public static GameConsole ConsoleInstance;

        public static void Log(string message, ConsoleColor color)
        {
            ConsoleInstance.Log(message, color);
        }

        public static void Log(string message, LogType log)
        {
            ConsoleInstance.Log(message, log);
        }
    }
}
