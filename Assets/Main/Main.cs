﻿using Assets.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Main
{
    public class Main : MonoBehaviour
    {
        public static string ServerInstanceName;
        public static string Terminal;

        void Awake()
        {
            DontDestroyOnLoad(gameObject);

            List<string> args = Environment.GetCommandLineArgs().ToList();

            ServerInstanceName = args[args.IndexOf(args.Where(a => a.Contains("name")).First()) + 1];
            Terminal = args[args.IndexOf(args.Where(a => a.Contains("terminal")).First()) + 1];

            Debug.Log(ServerInstanceName);

            gameObject.AddComponent<GameConsole>();
        }

    }
}
