#!/bin/bash

CONSOLE_STDIN=./$1/Console.STDIN
CONSOLE_STDOUT=./$1/Console.STDOUT
CONSOLE_STDIN_TEMPFILE=./$1/Console.STDIN_TEMP
trap "trap - SIGTERM && > $CONSOLE_STDOUT && > $CONSOLE_STDIN_TEMPFILE && > ./$1/Console.STDIN && kill -- -$$ " SIGINT SIGTERM EXIT

> $CONSOLE_STDIN_TEMPFILE
> $CONSOLE_STDOUT
> $CONSOLE_STDIN

./Unturned.x86 -batchmode -nographics -name $1 -terminal $2 > /dev/null &	

while true
do
	inotifywait -q -m -e close_write $CONSOLE_STDOUT | 
	while read path action file
	do
		INPUT_STRING=$(< $CONSOLE_STDIN_TEMPFILE)
		
		printf "\e[2K\r"
		printf "$(< $CONSOLE_STDOUT)\n"
		printf "${INPUT_STRING}"
	done
done &

while IFS= read -r -s -n 1 char 
do
    if [[ $char == $'\0' ]];     then
		INPUT_STRING=$(< $CONSOLE_STDIN_TEMPFILE)
        printf "$INPUT_STRING" >> $CONSOLE_STDIN
		> $CONSOLE_STDIN_TEMPFILE
    fi
    if [[ $char == $'\177' ]];  then
		INPUT_STRING=$(< $CONSOLE_STDIN_TEMPFILE)
		if [[ ${#INPUT_STRING} -ge 1 ]]; then
			INPUT_STRING="${INPUT_STRING::-1}"	
			printf "$INPUT_STRING" > $CONSOLE_STDIN_TEMPFILE
			printf "\e[2K\r"
			printf "${INPUT_STRING}"
		fi
    else
        INPUT_STRING=$(< $CONSOLE_STDIN_TEMPFILE)
		INPUT_STRING="${INPUT_STRING}${char}"	
		printf "${INPUT_STRING}" > $CONSOLE_STDIN_TEMPFILE
		printf "\e[2K\r"
		printf "${INPUT_STRING}"
    fi
done